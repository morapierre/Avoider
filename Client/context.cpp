#include "input.h"


Context::Context(int inxres,int inyres, int argc, char const *argv[])
{
	
	if (SDL_Init(SDL_INIT_VIDEO)!=0)  // initialise SDL
		return;
	TTF_Init();
	XRES = inxres;
	YRES = inyres;
	screen = SDL_SetVideoMode(XRES,YRES,32,SDL_HWSURFACE|SDL_DOUBLEBUF);    // initialise le mode vidéo
	//SDL_ShowCursor(0);  // 
	SDL_WM_SetCaption("Avoider+", NULL); 
	if (screen==NULL)
		return;
	if (LoadAll()!=0)       // charge les images
		return;

	police = TTF_OpenFont("Ttf/Juicebox.otf", 45);
	reseau = new Reseau(argc, argv);

}


int Context::LoadAll()
{
	int i;
	std::vector<std::string> all = {"Images/background2.png","Images/vaisseauJaune.png","Images/vaisseauBleu.png","Images/laserRouge.png",
									"Images/laserBleu.png","Images/bouleNeon.png","Images/bouleNeonVert.png"
									,"Images/attenteJoueur.jpg","Images/attenteGo.jpg","Images/attenteGo2.jpg"};
	nbimg = all.size() ;

	images = new SDL_Surface*[nbimg];  
	for(i=0;i<nbimg;i++)
	{
		images[i] = IMG_Load(all[i].c_str());
	}

	return 0;
}

void Context::Lancement()
{
	SDL_Rect positionRect ; positionRect.x=0;positionRect.y=0;
	SDL_BlitSurface(images[7], NULL, screen, &positionRect); 
	Flip();
	
	Reception() ;
	
	SDL_BlitSurface(images[8], NULL, screen, &positionRect); 
	Flip();
	Input in ;
	while(!lancementJeux(in))
	{
		in.Update() ;
	}
	// Envoi au serveur que le joueur est pret a jouer
	SDL_BlitSurface(images[9], NULL, screen, &positionRect); 
	Flip();
	char data[3];
	memset(&data, 0, sizeof(data));
	std::strcpy(data,"1") ;
	send(reseau->getServer(), (char*)&data, strlen(data), 0);
	
	

	Reception();
	
	compteARebour(screen, images,sprites) ;
}

void Context::Transmition(Input& in)
{
	
	memset(&dataSnd, 0, sizeof(dataSnd));
	std::strcpy(dataSnd,in.exportData().c_str()) ;
	send(reseau->getServer(), (char*)&dataSnd, strlen(dataSnd), 0);
}


void Context::Reception()
{
	
	sprites.clear() ;
	memset(&spriteRcv, 0, sizeof(spriteRcv));
	recv(reseau->getServer(), (char*)&spriteRcv, sizeof(spriteRcv), 0);
	std::string strSprites = std::string(spriteRcv);
	std::vector<std::string> tmp = splitString(strSprites);
	std::vector<int> vecSprites = stringToInt(tmp) ;

	
	Point p ;
	int imB ;
	for (int i = 0; i < vecSprites.size(); i+=4)
	{
		
		p.x = vecSprites[i+1] ; p.y = vecSprites[i+2] ;
		imB = vecSprites[i] ;
		
		sprites.push_back( Sprite(imB,p,0,0,vecSprites[i+3],
			0,images[imB]->w,images[imB]->h)) ;
	}

}


void Context::Render()
{
	SDL_Rect positionRect ; positionRect.x=0;positionRect.y=0;
	SDL_BlitSurface(images[0], NULL, screen, &positionRect); 

	for(auto s : sprites)
	{
		s.Render(images,screen) ;
	}
	SDL_Rect p ;
	
	SDL_Color blanc = {255, 255, 255};
	
    std::string timeStr = timeToString(SDL_GetTicks());
    SDL_Surface *timeSrf = TTF_RenderText_Solid(police, timeStr.c_str(), blanc);
    p.x = X_ecran / 2 - (timeSrf->w /2) ; p.y = Y_ecran - 46 ;
	SDL_BlitSurface(timeSrf, NULL, screen, &p);
	
}

void Context::Flip()
{
	while(SDL_Flip(screen)!=0)  // si la catre graphique n'est pas prête à blitter...
		SDL_Delay(1);			   // on attend un peu avant de réessayer : on reste ainsi synchro avec le balayage vertical
}

Context::~Context()
{
	int i;
	for(i=0;i<nbimg;i++)
		SDL_FreeSurface(images[i]);
	delete [] images;
	TTF_CloseFont(police);
	TTF_Quit();
	SDL_Quit();
}
