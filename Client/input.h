#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <SDL/SDL.h>
#include <unistd.h>
#include <math.h>
#include <chrono>
#include <thread>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_rotozoom.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <fstream>

#include <cstring>
#include <string>
#include <sstream>

#include  <iterator>
#include <vector>
#include <list>

#define X_ecran 1200
#define Y_ecran 750
#define Y_jeux  700

struct Point
{
	float x ;
	float y ;
};

typedef struct Point Point ;



class Input
{
protected:
	char key[SDLK_LAST];
	int mousex,mousey;
	int mousexrel,mouseyrel;
	char mousebuttons[8];
	char quit;
public:
	Input();
	~Input(){}
	void Update();
	inline char& Key(int i){return key[i];}
	inline int MouseX(){return mousex;}
	inline int MouseY(){return mousey;}
	inline int MouseXrel(){return mousexrel;}
	inline int MouseYrel(){return mouseyrel;}
	inline int MouseButton(int i){return mousebuttons[i];}
	std::string exportData();
	std::vector<int> importData(std::string str);
	inline int Quit(){return quit;}
};

class Sprite  // Un sprite, c'est un objet qui a une image et qui se déplace (Les vaisseaux joueur, les tirs, les boules) 
{
protected:
	int im;  // elle a une image (un index dans le tableau de surfaces)
	Point position ;
	float vx,vy;  // un vecteur de déplacement vx,vy 
	float angle;
	int vitesse;
	int hauteur ;
	int largeur ;
	
public:
	Sprite(){}
	Sprite(int inim,Point pst,int invx,int invy,float angl,int vit,int lg, int ht);
	~Sprite(){}
	inline void SetPosition(Point p){position.x= p.x;position.y= p.y;}
	inline void SetDimenssions(int l, int h){hauteur = h; largeur = l;}
	inline void EvolueFromVector(){position.x+=vx; position.y+=vy;}
	inline Point getPosition(){return position;}
	inline int getHauteur(){return hauteur;}
	inline float getAngle(){return angle;}
	inline float& Vx(){return vx;}
	inline float& Vy(){return vy;}
	
	void Render(SDL_Surface** images,SDL_Surface* screen);

	

};



class Reseau
{

protected:
	int serverSd;
	public:
	Reseau(){}
	Reseau(int argc, char const *argv[]);
	~Reseau();
	inline int getServer(){return serverSd;}


};


class Context  // Le contexte : c'est le contexte du jeu : tout ce qui définit le jeu.
{
protected:
	SDL_Surface* screen;    // la surface screen.
	Reseau *reseau ;
	SDL_Surface** images;   // tableau d'images
	int nbimg;              // nombre d'images (taille du tableau ci-dessus)
    std::vector<Sprite> sprites ;
	int XRES,YRES;   // résolution de la fenêtre
	int joueur ;
	TTF_Font *police ;
	char spriteRcv[2000] ;
	char dataSnd[30];
	int tempsActuel;
	int tempsPrecedent;
private:
	int LoadAll();

public:
	Context(int inxres,int inyres,int argc, char const *argv[]);
	~Context();
	void Lancement();
	void Transmition(Input&);
	void Reception();
	void Render();
	void Flip();
};


std::vector<std::string> splitString(std::string str);
std::vector<int> stringToInt(std::vector<std::string> vec);
std::string timeToString(int timeA);
bool lancementJeux(Input& in);
void compteARebour(SDL_Surface *screen, SDL_Surface **images,std::vector<Sprite> sprites);