#include "input.h"

#define TEMPS  2

int main(int argc, char const *argv[])

{
  
  Context C(X_ecran,Y_ecran,argc,argv);

  Input in ; 

  C.Lancement() ;

  SDL_Delay(10);
    
  while(!in.Key(SDLK_ESCAPE) && !in.Quit())
  {
    
      in.Update();
      C.Transmition(in); // Envoie au serveur des input du joueur
      C.Reception();     // Réponse du serveur ( emplacements des elements de jeux )
      C.Render();
      C.Flip(); 
      
  }
  
  return 0 ;
        
  }


  

      