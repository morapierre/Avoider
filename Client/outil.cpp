#include "input.h"


std::vector<std::string> splitString(std::string str){

std::stringstream ss(str);
std::istream_iterator<std::string> begin(ss);
std::istream_iterator<std::string> end;
std::vector<std::string> results(begin, end);

return results ;
}

std::vector<int> stringToInt(std::vector<std::string> vec){
  
std::vector<int> results ;
std::string::size_type sz;
for(auto vst : vec){
  results.push_back(std::stoi (vst,&sz)) ;
}

  return results ;
}

std::string timeToString(int timeA)
{
	timeA/=1000; 
	int minute = timeA/60 ;
	int seconde = timeA - (minute * 60) ;

	std::string secStr , minStr, res ;

	secStr = std::to_string(seconde) ;
	if (seconde < 10 )
		secStr = "0"+ secStr ;
	
	minStr = std::to_string(minute);
	if (minute < 10)
		minStr = "0"+minStr ;

	res = minStr+":"+secStr ;
	return res ;

}

float distanceEuclidienne(Point p1, Point p2)
{
	return sqrt(pow(p1.x-p2.x,2)+pow(p1.y-p2.y,2)) ;
}

bool lancementJeux(Input& in)
{
	Point p1,p2 ;
	p1.x = in.MouseX() ; p1.y= in.MouseY() ;
	p2.x = 600; p2.y = 465;
	return ((distanceEuclidienne(p1,p2)<(130/2))&&(in.MouseButton(SDL_BUTTON_LEFT))) ;
}

void compteARebour(SDL_Surface *screen, SDL_Surface **images,std::vector<Sprite> sprites)
{
	SDL_Rect p,p1 ;
	SDL_Color blanc = {255, 255, 255};
	TTF_Font *police = TTF_OpenFont("Ttf/Juicebox.otf", 65);
    SDL_Surface *timeSrf = NULL ;
	p1.x = 0 ; p1.y  = 0 ;
	for (int i = 3; i >=0 ; --i)
	{
		SDL_BlitSurface(images[0], NULL, screen, &p1); 
		for(auto s : sprites)
			s.Render(images,screen) ;
		if (i>0)
			timeSrf = TTF_RenderText_Solid(police, std::to_string(i).c_str(), blanc);
		else
			timeSrf = TTF_RenderText_Solid(police, "GO", blanc);

		p.x = X_ecran / 2 - (timeSrf->w /2) ; p.y = Y_jeux/2 - (timeSrf->h /2);
		SDL_BlitSurface(timeSrf, NULL, screen, &p);
		SDL_Flip(screen);
		if (i>0)
			SDL_Delay(1000);
		else
			SDL_Delay(400);
	}

}