#include "input.h"
using namespace std;
Reseau::Reseau(int argc, char const *argv[]){
  //for the server, we only need to specify a port number
  int port ;

   if (argc == 3)
    {
        char const *serverIp = argv[1]; 
        port = atoi(argv[2]); 
        
        //setup a socket and connection tools 
        struct hostent* host = gethostbyname(serverIp); 
        sockaddr_in sendSockAddr;   
        bzero((char*)&sendSockAddr, sizeof(sendSockAddr)); 
        sendSockAddr.sin_family = AF_INET; 
        sendSockAddr.sin_addr.s_addr = 
        
        inet_addr(inet_ntoa(*(struct in_addr*)*host->h_addr_list));
        sendSockAddr.sin_port = htons(port);
        serverSd = socket(AF_INET, SOCK_STREAM, 0);
        //try to connect...
        int status = connect(serverSd,
                             (sockaddr*) &sendSockAddr, sizeof(sendSockAddr));
        if(status < 0)
        {
            cout<<"Erreur de connection a la socket!"<<endl; exit(0) ;
        }
        cout << "Connecte au Server!" << endl<< "let's GO !" << endl;
    }

    else
    {
      cerr << "Erreur parrametres" << endl;
      exit(0);
    }
} 
    
Reseau::~Reseau()
{
    close(serverSd);    
}