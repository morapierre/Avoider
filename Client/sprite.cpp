#include "input.h"
Sprite::Sprite(int inim,Point pst,int invx,int invy,float angl,int vit,int lg , int ht)
{
	im=inim;
	position = pst ;
	vx=invx;
	vy=invy;
	angle = angl;
    vitesse = vit ;
    largeur = lg ;
    hauteur = ht ;
}

void Sprite::Render(SDL_Surface** images,SDL_Surface* screen)
{
	SDL_Rect positionRect ; 
    SDL_Surface *rotation ;
    rotation = rotozoomSurface(images[im], (360-angle)-90, 1.0, 1) ;

    positionRect.x = position.x - rotation->w / 2; positionRect.y = position.y - rotation->h / 2 ;
	SDL_BlitSurface(rotation, NULL, screen, &positionRect); 

    SDL_FreeSurface(rotation); 
}

