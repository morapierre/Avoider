Simple 2 player game.
Rules : 
    You have to avoid the balls and shoot the other player, the one who stays alive wins.

Compilation :
First : install needed librairies with : 
	sudo apt-get install libsdl-gfx1.2-5 libsdl-gfx1.2-dev libsdl1.2-dev libsdl1.2debian libsdl-image1.2 libsdl-image1.2-dev
Then :
You have to type "make" on the two following directories : Client, Server 

Execution :
- Run the server program : ./Avoider+ port_number
choose a port_number available 50000 for example
      
- each player runs the client program : ./Avoider+ address_server port_number
    
    