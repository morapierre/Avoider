#include "input.h"

Boule::Boule(int inim,Point pst,int invx,int invy,float angl,int vit,int lg, int ht,int v):
			Sprite(inim,pst,invx,invy,angl,vit,lg,ht), vie{v} {

	if (angle == 400)
		angle = rand_a_b(0,361) - 180 ;
	if (vitesse == -1)
		vitesse = rand_a_b(2,5) ;

	collisionBoules = false ;

	CalculVecteurDeplacement();
}

void Boule::GetCollisionTerrain(){

	if ((position.x - largeur/2 <= 0 )||(position.x + largeur/2 >= X_ecran ))
	{
		vx *= -1 ;
	}
	else if ((position.y - hauteur/2 <= 0 )||(position.y + hauteur/2 >= Y_jeux ))
	{
		vy *= -1 ; 
	}
}

bool Boule::setCollisionBoules(std::vector<Boule> boules)
{
	bool res = true ;

	Point p1 ;
	p1.x = X_ecran/2;
	p1.y = Y_jeux/2 ;
	for(auto b:boules)
	{
		if ((b.getPosition().x != position.x)&&(b.getPosition().y != position.y))
			res = res && (distanceEuclidienne(position,b.getPosition()) >
					 (hauteur/2) + (b.getHauteur()/2)) ;
					 
	}
	
	res = res && (distanceEuclidienne(position,p1)>(hauteur/2)+50) ;
	return res ;
}

void Boule::GetCollisionBoules(std::vector<Boule> boules,int ind)
{
	
	int i ;
	Point po, p, N;
	if (!collisionBoules)
		collisionBoules = boules[ind].setCollisionBoules(boules) ;

	p.x = X_ecran/2; p.y = Y_jeux /2;
	boules.push_back(Boule(0,p,0,0,0,0,100,100,10));
	for (int i = 0; i < boules.size(); ++i)
	{
		if ((i != ind)&&(collisionBoules))
			{
				p = boules[i].getPosition() ;
				if (distanceEuclidienne(position,p) < (hauteur/2) + (boules[i].getHauteur()/2))
				{
					N.x = position.x - p.x ;
  					N.y = position.y - p.y ;

    				float norme = sqrt(N.x*N.x + N.y*N.y);
  					if (norme != 0){
    					N.x/=norme;
						N.y/=norme;
					}
					po.x = vx ;
					po.y = vy ;
					Point v2 ;
					float pscal = (vx*N.x +  vy*N.y);
					vx = vx -2*pscal*N.x;
					vy = vy -2*pscal*N.y;
					collisionBoules = false ;
					
				}
			}
	}

}

std::string Boule::exportDataBoule(){
	

	std::string rtn = std::to_string((int)position.x) + " " + std::to_string((int)position.y) + " " + 
					  std::to_string((int)angle)+ " " + std::to_string(vitesse) + " " + 
					  std::to_string(im) +" " + std::to_string(vie) ;

	return (rtn) ;

}