#include "input.h"


Context::Context(int nbVs,int nbBl, int argc, char const *argv[])
{
	
	LoadAll() ;      // charge les images

	nbvaisseau = nbVs;
	InitVaisseaux();   // initialise les vaisseaux

	reseau = new Reseau(argc, argv);
	
	usleep(2000);
	Transmition() ;

	std::cout<<"Avant att"<<std::endl;
	attenteClient(reseau->getJ1(),reseau->getJ2());// reseau->getJ2());

	usleep(2000);
	Transmition() ;

	nbBoules = nbBl; 
	InitBoules();

	partie.nbBoules = nbBl ;
	partie.delaiBoule = 5 ;in[1].flecheG = 0;
	in[1].flecheD = 0;
	in[1].toucheF = 0;

}

int Context::LoadAll()
{
	int i;
	std::vector<std::string> all = {"Images/background2.png","Images/vaisseauJaune.png","Images/vaisseauBleu.png","Images/laserRouge.png",
									"Images/laserBleu.png","Images/bouleNeon.png","Images/bouleNeonVert.png"};
	nbimg = all.size() ;

	images = new SDL_Surface*[nbimg];  
	for(i=0;i<nbimg;i++)
	{
		images[i] = IMG_Load(all[i].c_str());
	}
	return 0;
}

void Context::InitVaisseaux()
{
	int i;
	Point p ;
	vaisseaux = new Vaisseau[nbvaisseau];

	p.x =50 ; p.y = Y_jeux / 2 ;
	vaisseaux[0] = Vaisseau(1,p,0,0,0,5,images[1]->w,images[1]->h,3,1) ; 

	p.x = X_ecran -50 ; p.y = Y_jeux / 2 ;
	vaisseaux[1] = Vaisseau(2,p,0,0,180,5,images[2]->w,images[2]->h,3,2) ;
	
}

void Context::InitBoules(){
	Point p ;
	int couleur ;
	p.x = X_ecran/2; p.y = Y_jeux/2 ;
	
	for (int i = 0; i < nbBoules; ++i)
	{
		couleur=rand_a_b(5,7);
		boulesJeux.push_back(Boule(couleur,p,0,0,400,-1,images[couleur]->w,images[couleur]->h,10)) ;
	}

}

void Context::Reception()
{
	
	memset(&dataRcv, 0, sizeof(dataRcv));
	recv(reseau->getJ1(), (char*)&dataRcv, sizeof(dataRcv), 0);

	std::string strIn = std::string(dataRcv);
	std::vector<std::string> tmp = splitString(strIn);
	std::vector<int> vecInput = stringToInt(tmp) ;
	in[0].flecheG = vecInput[0];
	in[0].flecheD = vecInput[1];
	in[0].toucheF = vecInput[2];
	

	memset(&dataRcv, 0, sizeof(dataRcv));
	recv(reseau->getJ2(), (char*)&dataRcv, sizeof(dataRcv), 0);

	strIn = std::string(dataRcv);
	tmp = splitString(strIn);
	vecInput = stringToInt(tmp) ;
	in[1].flecheG = vecInput[0];
	in[1].flecheD = vecInput[1];
	in[1].toucheF = vecInput[2];
	

}

void Context::Calcul()
{
	Point positionJoueur ;
	tempsActuel = std::clock() ;
	for (int i = 0; i < 2; ++i)
	{
		positionJoueur = vaisseaux[i].getPosition() ;
		vaisseaux[i].CalculAngleClavier(in[i].flecheG,in[i].flecheD) ;


		if (tempsActuel - vaisseaux[i].getTempsPreced() > vaisseaux[i].getDelaiTir())
		{
			vaisseaux[i].setTempsPreced(tempsActuel) ;
			vaisseaux[i].setTirer(true) ;
		}

		if ((in[i].toucheF)&&(vaisseaux[i].getTirer()))
		{
			Point pIni, pObj ;
			pObj = vaisseaux[i].getNezVaisseau(40) ;
			pIni = vaisseaux[i].getNezVaisseau(0) ;
			Tir tir =  Tir(i+3,pIni,0,0,0,10,images[i+3]->w, images[i+3]->h,pObj) ;
		
			vaisseaux[i].ajouterTir(tir) ;
			vaisseaux[i].setTirer(false) ;
		}

		// REMPLISSAGE DES NOUVELLES COORDONNÉES DANS LES TABLEAU POUR LES COLLISIONS

		vaisseaux[i].remplirTableau();

		vaisseaux[i].remplirTableauTirs() ;


		// GESTION DES COLLISIONS

		vaisseaux[i].CollisionTerrainTirs();

		vaisseaux[i].CollisionTirVaisseau(vaisseaux[1-i].getTabPoint());

		vaisseaux[i].CollisionBouleVaisseau(boulesJeux);

		vaisseaux[i].CollisionVaisseauVaisseau(vaisseaux[1-i].getTabPoint()) ;

	    std::vector<int> bouleTouches = vaisseaux[i].CollisionTirBoule(boulesJeux); 

		for (int i = 0; i < boulesJeux.size(); ++i)
		{
			boulesJeux[i].GetCollisionTerrain();
			boulesJeux[i].GetCollisionBoules(boulesJeux,i) ;
			boulesJeux[i].EvolueFromVector();
		}


		// Evolution Tir et Vaisseau

		vaisseaux[i].EvolueTir();

		if (!(vaisseaux[i].CollisionTerrain()))
			vaisseaux[i].EvolueFromVector() ;
		
		EvolutionPartie(bouleTouches);

	}
}

void Context::EvolutionPartie(std::vector<int> bouleTouches)
{
	for (auto b : bouleTouches)
	{
		boulesJeux.erase(boulesJeux.begin()+b) ;
	}
	
	int couleur ;
	Point p ;
	p.x = X_ecran/2 ; p.y = Y_jeux / 2 ;

	for (int i = boulesJeux.size(); i <= partie.nbBoules; ++i)
	{
		std::cout<<i<<std::endl;
		couleur=rand_a_b(5,7);
		boulesJeux.push_back(Boule(couleur,p,0,0,400,-1,images[couleur]->w,images[couleur]->h,10)) ;
	}
}


void Context::Transmition(){

	std::string transmition = "" ;
	int i ;
	// Vaisseau et tirs
	for (i = 0; i < 2; ++i)
	{
		if (i!=0) transmition += " " ;

		transmition += vaisseaux[i].exportSprite() ;

		for (auto t : vaisseaux[i].getTirs())
		{
			transmition += " " +t.exportSprite() ;
		}
	}

	for(auto b : boulesJeux)
	{
		transmition += " " + b.exportSprite() ;
	}

	memset(&dataSnd, 0, sizeof(dataSnd));
	std::strcpy(dataSnd,transmition.c_str()) ;

	send(reseau->getJ1(), (char*)&dataSnd, strlen(dataSnd), 0);
	send(reseau->getJ2(), (char*)&dataSnd, strlen(dataSnd), 0);


}


Context::~Context()
{
	int i;
	for(i=0;i<nbimg;i++)
		SDL_FreeSurface(images[i]);
	delete [] images;
	delete [] vaisseaux;
	SDL_Quit();
}
