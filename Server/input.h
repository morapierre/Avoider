#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <SDL/SDL.h>
#include <unistd.h>
#include <math.h>
#include <chrono>
#include <thread>
#include <SDL/SDL_image.h>
#include <SDL/SDL_rotozoom.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <fstream>
#include <omp.h>

#include <cstring>
#include <string>
#include <sstream>

#include  <iterator>
#include <vector>
#include <list>

#define X_ecran 1200
#define Y_ecran 750
#define Y_jeux 700

struct Point
{
	float x ;
	float y ;
};

typedef struct Point Point ;

struct Equation
{
	float a ;
	float b ;
};
typedef struct Equation Equation ;

struct Input
{
	bool flecheG ;
	bool flecheD ;
	bool toucheF ;
};
typedef struct Input Input ;

struct Partie
{
	int nbBoules;
	int delaiBoule;
	int tempsBoule;
};
typedef struct Partie Partie; 

class Sprite  // Un sprite, c'est un objet qui a une image et qui se déplace (Les vaisseaux joueur, les tirs, les boules) 
{
protected:
	int im;  // elle a une image (un index dans le tableau de surfaces)
	Point position ;
	float vx,vy;  // un vecteur de déplacement vx,vy 
	float angle;
	int vitesse;
	int hauteur ;
	int largeur ;
	
public:
	Sprite(){}
	Sprite(int inim,Point pst,int invx,int invy,float angl,int vit,int lg, int ht);
	~Sprite(){}
	inline void SetPosition(Point p){position.x= p.x;position.y= p.y;}
	inline void SetDimenssions(int l, int h){hauteur = h; largeur = l;}
	inline void EvolueFromVector(){position.x+=vx; position.y+=vy;}
	inline Point getPosition(){return position;}
	inline int getHauteur(){return hauteur;}
	inline float getAngle(){return angle;}
	inline float& Vx(){return vx;}
	inline float& Vy(){return vy;}
	
	void Render(SDL_Surface** images,SDL_Surface* screen);
	void CalculAngle(Point p2);
	void CalculAngleClavier(bool left,bool right) ;
	void CalculVecteurDeplacement();

	std::string exportSprite() ;

	

};

class Boule : public Sprite{
private:
	int vie ;
	bool collisionBoules ;
public:
	Boule(){}
	Boule(int inim,Point pst,int invx,int invy,float angl,int vit,int lg, int ht,int v);
	~Boule(){}

	void GetCollisionTerrain();
	void GetCollisionBoules(std::vector<Boule> boules,int ind);
	bool setCollisionBoules(std::vector<Boule> boules);
	std::string exportDataBoule();


};

class Tir : public Sprite{
private:
	std::vector<Point> tabPoints ;
	Point pointObjectif ;
	bool calculTrajectoire ;
public:
	Tir(){}
	Tir(int inim,Point pst,int invx,int invy,float angl,int vit,int lg, int ht, Point obj);
	~Tir(){}
	inline Point getPointObj(){return pointObjectif;}
	inline bool getCalculTraj(){return calculTrajectoire;}
	inline bool setCalculTraj(bool b){calculTrajectoire = b ;}
	inline std::vector<Point> getTabPoint(){return tabPoints;}
	void remplirTableau();
	bool CollisionTerrain();

};

class Vaisseau : public Sprite{
private:
	std::vector<Point> tabPoints ;
	bool tirer ;
	bool collisionVaisseau ;
	int tempsPreced ;
	int delaiTir ;
	int vie ;
	int numJoueur ;
	std::vector<Tir> tirs;

public:
	Vaisseau(){}
	Vaisseau(int inim,Point pst,int invx,int invy,float angl,int vit,int lg,int ht, int v, int nJ);
	~Vaisseau(){}
	Point getNezVaisseau(int ajout);
	inline void ajouterTir( Tir t){ tirs.push_back(t);}
	inline int getNbTir(){return tirs.size() ;}
	inline bool getTirer(){return tirer ;}
	inline int getDelaiTir(){return delaiTir ;}
	inline int getTempsPreced(){return tempsPreced;}
	inline void setDelaiTir(int tmp){delaiTir = tmp;} 
	inline void setTempsPreced(int tmp){tempsPreced = tmp;} 

	inline void setTirer(bool t){tirer = t;} 
	inline std::vector<Tir> getTirs(){return tirs ;}
	inline std::vector<Point> getTabPoint(){return tabPoints;}
	
	void remplirTableau();
	void remplirTableauTirs();
	void RenderAll(SDL_Surface** images,SDL_Surface* screen);
	void EvolueTir();

	bool CollisionTerrain();
	void CollisionTerrainTirs();
	int CollisionTirVaisseau(std::vector<Point> vaisseauEnnemie);
	void CollisionBouleVaisseau(std::vector<Boule> boules);
	bool CollisionVaisseauVaisseau(std::vector<Point> autreV);
	std::vector<int> CollisionTirBoule(std::vector<Boule> boules);


};


class Reseau
{

protected:
	int joueur1Sd ;
	int joueur2Sd ;
	int serverSd ;
	public:
	Reseau(){}
	Reseau(int argc, char const *argv[]);
	~Reseau();
	inline int getJ1(){return joueur1Sd;}
	inline int getJ2(){return joueur2Sd;}

};


class Context  // Le contexte : c'est le contexte du jeu : tout ce qui définit le jeu.
{
protected:
	Reseau *reseau ;
	int nbimg; 
	SDL_Surface** images;   
	int nbvaisseau;      
	Vaisseau* vaisseaux;  
	int nbBoules  ;
	std::vector<Boule> boulesJeux ;
	Partie partie ;
	     
	char dataRcv[30];
	char dataSnd[2000];

	int tempsActuel ;

	Input in[2];

private:
	int LoadAll();
	void InitVaisseaux();
	void InitBoules();
	void EvolutionPartie(std::vector<int> bouleTouches);

public:
	Context(int nbVs,int nbBl, int argc, char const *argv[]);
	~Context();
	void Reception();
	void Calcul();
	void Transmition();
};


float distanceEuclidienne(Point p1, Point p2) ;
float CalculAngleDeuxPoint(Point p1, Point p2);
Point rotationPoint(Point p,Point ref, float angle);
int rand_a_b(int a, int b);
Point getPointSurDroite(int distance, Point p1, Point p2);
bool CollisionSegment(Point A,Point B,Point C, float rayon);
bool CollisionSegSeg(Point A,Point B,Point O,Point P);

std::vector<std::string> splitString(std::string str);
std::vector<int> stringToInt(std::vector<std::string> vec);
void attenteClient(int j1, int j2);
