#include "input.h"

#define TEMPS  100

int main(int argc, char const *argv[])

{
  int tempsPrecedent = std::clock(), 
      tempsActuel = std::clock();
  srand((unsigned int)time(NULL));

  Context C(2,4,argc,argv);
    
  
  while(1)
  {

      C.Reception() ;
      C.Calcul();
      C.Transmition();

       
      tempsActuel = std::clock();

        if (tempsActuel - tempsPrecedent > TEMPS)
            tempsPrecedent = tempsActuel;
       else
          usleep((TEMPS - (tempsActuel - tempsPrecedent))*1000);
  
  
  }
    return 0 ;
        
  }


  

      