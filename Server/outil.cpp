#include "input.h"



float CalculAngleDeuxPoint(Point p1, Point p2){
	float xf,yf,angle;

    xf=((p2.x-p1.x)/(sqrt(pow(p2.x-p1.x,2)+pow(p2.y-p1.y,2))));
    yf=((p2.y-p1.y)/(sqrt(pow(p2.x-p1.x,2)+pow(p2.y-p1.y,2))));

    angle = acos(xf);


    if ((p2.y-p1.y)>=0)
        angle = angle*(180/M_PI);
    else
		angle = (-angle)*(180/M_PI);

	return angle ;
        
}


Point rotationPoint(Point p,Point ref, float angle){
angle = angle * M_PI / 180;
	Point result ;
	p.x -= ref.x ;
	p.y -= ref.y ;
	result.x = (p.x) * cos(angle) - (p.y) * sin(angle);
	result.y = (p.x) * sin(angle) + (p.y) * cos(angle);
	result.x += ref.x ;
	result.y += ref.y ;

return result ;
}

int rand_a_b(int a, int b){
    return rand()%(b-a) +a ;
}

Point getPointSurDroite(int distance, Point p1, Point p2)
{
	float angle = CalculAngleDeuxPoint(p1,p2);

	Point res ;

	res.x = p1.x +(cos((angle) *M_PI/180)*distance); 
    res.y = p1.y +(sin((angle) *M_PI/180)*distance);

    return res ;
}

float distanceEuclidienne(Point p1, Point p2)
{
	return sqrt(pow(p1.x-p2.x,2)+pow(p1.y-p2.y,2)) ;
}

bool CollisionDroite(Point A,Point B,Point C, float rayon)

{

   Point u;

   u.x = B.x - A.x;
   u.y = B.y - A.y;

   Point AC;

   AC.x = C.x - A.x;
   AC.y = C.y - A.y;

   float numerateur = u.x*AC.y - u.y*AC.x;   // norme du vecteur v

   if (numerateur <0)

      numerateur = -numerateur ;   // valeur absolue ; si c'est négatif, on prend l'opposé.

   float denominateur = sqrt(u.x*u.x + u.y*u.y);  // norme de u

   float CI = numerateur / denominateur;

   if (CI< rayon)

      return true;

   else

      return false;

}


bool CollisionSegment(Point A,Point B,Point C, float rayon)

{

   if (CollisionDroite(A,B,C,rayon) == false)

     return false;  // si on ne touche pas la droite, on ne touchera jamais le segment

   Point AB,AC,BC;

   AB.x = B.x - A.x;
   AB.y = B.y - A.y;

   AC.x = C.x - A.x;
   AC.y = C.y - A.y;

   BC.x = C.x - B.x;
   BC.y = C.y - B.y;

   float pscal1 = AB.x*AC.x + AB.y*AC.y;  // produit scalaire

   float pscal2 = (-AB.x)*BC.x + (-AB.y)*BC.y;  // produit scalaire

   if (pscal1>=0 && pscal2>=0)

      return true;   // I entre A et B, ok.

   // dernière possibilité, A ou B dans le cercle

   if (distanceEuclidienne(A,C) <= rayon)

     return true;

   if (distanceEuclidienne(B,C) <= rayon)

     return true;

   return false;

}


bool CollisionDroiteSeg(Point A,Point B,Point O,Point P)

{

  Point AO,AP,AB;

  AB.x = B.x - A.x;
  AB.y = B.y - A.y;

  AP.x = P.x - A.x;
  AP.y = P.y - A.y;

  AO.x = O.x - A.x;
  AO.y = O.y - A.y;

  if ((AB.x*AP.y - AB.y*AP.x)*(AB.x*AO.y - AB.y*AO.x)<0)

     return true;

  else

     return false;

}

bool CollisionSegSeg(Point A,Point B,Point O,Point P)

{

  if (CollisionDroiteSeg(A,B,O,P)==false)

     return false;  // inutile d'aller plus loin si le segment [OP] ne touche pas la droite (AB)

  if (CollisionDroiteSeg(O,P,A,B)==false)

     return false;

  return true;

}

std::vector<std::string> splitString(std::string str){

std::stringstream ss(str);
std::istream_iterator<std::string> begin(ss);
std::istream_iterator<std::string> end;
std::vector<std::string> results(begin, end);

return results ;
}

std::vector<int> stringToInt(std::vector<std::string> vec){
  
std::vector<int> results ;
std::string::size_type sz;
for(auto vst : vec){
  results.push_back(std::stoi (vst,&sz)) ;
}

  return results ;
}


void attenteClient(int j1, int j2){

  char data1[3],data2[3];
  memset(&data1, 0, sizeof(data1));
  memset(&data2, 0, sizeof(data2));

omp_set_num_threads(2);
#pragma omp parallel 
{
  int id = omp_get_thread_num();
  if (id==0)
  {
    std::cout<<"attente J1 "<<std::endl;
    recv(j1, (char*)&data1, sizeof(data1), 0);
    std::cout<<"Recu J1 "<<std::endl;
  }
  else
  {
    std::cout<<"attente J2 "<<std::endl;
    recv(j2, (char*)&data2, sizeof(data2), 0);
    std::cout<<"Recu J2 "<<std::endl;
  }
  
}

std::cout<<"fin T"<<std::endl ;
}
