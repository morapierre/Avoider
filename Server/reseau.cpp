#include "input.h"
using namespace std;
Reseau::Reseau(int argc, char const *argv[]){
 
  int port ;
  
    if (argc == 2)
    {
          
        port = atoi(argv[1]);
 
        sockaddr_in servAddr;
        bzero((char*)&servAddr, sizeof(servAddr));
        servAddr.sin_family = AF_INET;
        servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
        servAddr.sin_port = htons(port);
     

        serverSd = socket(AF_INET, SOCK_STREAM, 0);
        if(serverSd < 0)
        {
            cerr << "Error establishing the server socket" << endl;
            exit(0);
        }
 
        int bindStatus = bind(serverSd, (struct sockaddr*) &servAddr, 
            sizeof(servAddr));
        if(bindStatus < 0)
        {
            cerr << "Error binding socket to local address" << endl;
            exit(0);
        }
        cout << "Attente des deux joueurs" << endl;
        //listen for up to 5 requests at a time
        listen(serverSd, 5);
        //receive a request from client using accept
        //we need a new address to connect with the client
        sockaddr_in newSockAddr;
        socklen_t newSockAddrSize = sizeof(newSockAddr);
        //accept, create a new socket descriptor to 
        //handle the new connection with client

        joueur1Sd = accept(serverSd, (sockaddr *)&newSockAddr, &newSockAddrSize);
        if(joueur1Sd < 0)
        {
            cerr << "Erreur requete du joueur !" << endl;
            exit(1);
        }
        cout << "Joueur 1 connecte !" << endl << "Attente du second Joueur" << endl;

        joueur2Sd = accept(serverSd, (sockaddr *)&newSockAddr, &newSockAddrSize);
        if(joueur2Sd < 0)
        {
            cerr << "Erreur requete du joueur !" << endl;
            exit(1);
        }
        cout << "Joueur 2 connecte !" << endl << "Debut de la partie" << endl;

       
    }

      
    else
    {
      cerr << "Erreur parrametres" << endl;
      exit(0);
    }
} 
    
Reseau::~Reseau()
{
    close(joueur1Sd);
    close(joueur2Sd);
    close(serverSd);
   
 
    
}