#include "input.h"
Sprite::Sprite(int inim,Point pst,int invx,int invy,float angl,int vit,int lg , int ht)
{
	im=inim;
	position = pst ;
	vx=invx;
	vy=invy;
	angle = angl;
    vitesse = vit ;
    largeur = lg ;
    hauteur = ht ;
}

void Sprite::Render(SDL_Surface** images,SDL_Surface* screen)
{
	SDL_Rect positionRect ; 
    SDL_Surface *rotation ;
    rotation = rotozoomSurface(images[im], (360-angle)-90, 1.0, 1) ;

    positionRect.x = position.x - rotation->w / 2; positionRect.y = position.y - rotation->h / 2 ;
	SDL_BlitSurface(rotation, NULL, screen, &positionRect); 

    SDL_FreeSurface(rotation); 
}

void Sprite::CalculAngle( Point p2)
{
   
    angle = CalculAngleDeuxPoint(position, p2) ;

    vx = (cos((angle) *M_PI/180)*vitesse); 
    vy =(sin((angle) *M_PI/180)*vitesse);
}

void Sprite::CalculAngleClavier(bool left,bool right){
    if ((left)&&!(right))
    {
        if (angle - 3.5 < -180)
            angle = 180 ;
        else
            angle-=3.5;
    }
    else if ((right)&&(!left))
    {
        if (angle + 3.5 > 180)
            angle = -180 ;
        else
            angle+=3.5;
    }

    CalculVecteurDeplacement();
}

void Sprite::CalculVecteurDeplacement()
{
    vx = (cos((angle) *M_PI/180)*vitesse); 
    vy =(sin((angle) *M_PI/180)*vitesse);
}


std::string Sprite::exportSprite(){

    std::string rtn = std::to_string(im) + " " + std::to_string((int)position.x) + 
                        " " + std::to_string((int)position.y) + " " +
                        std::to_string((int)angle) ;

    return (rtn) ;
}