#include "input.h"

Tir::Tir(int inim,Point pst,int invx,int invy,float angl,int vit,int lg, int ht, Point obj):
		Sprite(inim, pst, invx, invy, angl, vit, lg,  ht), pointObjectif{obj}, calculTrajectoire{true} {
			
			CalculAngle(obj) ;
		}

void Tir::remplirTableau(){
	tabPoints.clear();
	Point p , ref, rotation;
	ref.x = position.x ;
	ref.y = position.y ;
	p.x = position.x - largeur / 2 ;
	p.y = position.y - hauteur / 2 ;
	rotation = rotationPoint(p,ref,angle+90);
	tabPoints.push_back(rotation);

	p.x += largeur ;
	rotation = rotationPoint(p,ref,angle+90);
	tabPoints.push_back(rotation) ;

}

bool Tir::CollisionTerrain(){
	Point p1 ;
	p1.x = X_ecran/2 ;
	p1.y = Y_jeux/2 ;
	for (auto p : tabPoints)
	{
		if (!(((p.x /*+ vx*/ ) > 0) && ((p.x /*+ vx*/ ) < X_ecran)
			&& ((p.y /*+ vy*/ ) > 0) && ((p.y /*+ vy*/ ) < Y_jeux)
			&& (distanceEuclidienne(p,p1)>50)))
			return true ;
	}
	
	return false ;
}