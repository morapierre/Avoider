#include "input.h"

Vaisseau::Vaisseau(int inim,Point pst,int invx,int invy,float angl,int vit,int lg,int ht, int v, int nJ): 
				Sprite(inim,pst,invx,invy,angl,vit,lg,ht), vie{v}, numJoueur{nJ}{
					collisionVaisseau = false ;
					delaiTir = 3500 ;
					remplirTableau();
				}



Point Vaisseau::getNezVaisseau(int ajout){
		Point p ;
		p.x = position.x + (cos((angle) *M_PI/180)*((hauteur/2)+ajout)); 
    	p.y = position.y + (sin((angle) *M_PI/180)*((hauteur/2)+ajout));
    	return p ;
	}




void Vaisseau::remplirTableau(){

	tabPoints.clear();
	Point p ,ref, rotation;
	ref.x = position.x ; ref.y = position.y ;

	p.x = position.x + largeur / 2;
	p.y = position.y + hauteur / 2  ;
	rotation = rotationPoint(p,ref,angle+90) ;
	tabPoints.push_back(rotation) ;

	p.x = position.x - (largeur / 2) - 7;
	p.y = position.y + (hauteur / 2);
	rotation = rotationPoint(p,ref,angle+90);
	tabPoints.push_back(rotation) ;

	

	p.x = position.x ;
	p.y = position.y - hauteur / 2 ;
	rotation = rotationPoint(p,ref,angle+90) ;
	tabPoints.push_back(rotation) ;

}



void Vaisseau::remplirTableauTirs(){
	for (std::vector<Tir>::iterator t = tirs.begin();
	        t!= tirs.end(); t++)
		t->remplirTableau() ;
}

bool Vaisseau::CollisionTerrain(){
	Point p1;
	p1.x = X_ecran/2;
	p1.y = Y_jeux/2;
	return !( ((position.x + vx - largeur/2) > 0) && 
			  ((position.x + vx + largeur/2) < X_ecran) &&
			  ((position.y + vy - hauteur/2) > 0) &&
			  ((position.y + vy + hauteur /2) < Y_jeux )
			  &&(distanceEuclidienne(position,p1)>hauteur/2+50) ) ;

	
}

void Vaisseau::CollisionTerrainTirs(){
	int i = 0 ;
	std::vector<int> tirDetruits ;
	for (auto t : tirs)
	{
		if (t.CollisionTerrain())
		{
			tirDetruits.push_back(i) ; 
		}
			
		i++ ;
	}

	for (auto ind : tirDetruits)
	{
		tirs.erase(tirs.begin()+ind);
	}

}

int Vaisseau::CollisionTirVaisseau(std::vector<Point> vaisseauEnnemie){
	int collisions = 0 ;
	int indTir = 0 , i ;
	bool toucher ;
	Point A, B, D, T ;
	std::vector<int> tirDetruits ;
	for (auto t : tirs) // Pour tout les tirs
	{
		 toucher = true ;
		 for (auto P : t.getTabPoint()) // Pour les 2 points représentant le tir
		 {
		 			//P.x = 950; P.y = 300;
		 	for(i=0; (i< vaisseauEnnemie.size()) /*&& ( !toucher )*/;i++) // on regarde si il y en a au moins 1 dans le vaisseau
			{
						
			      A = vaisseauEnnemie[i];//std::cout<<A.x<<"  "<<A.y<<std::endl ; 

			     if (i==vaisseauEnnemie.size()-1)  // si c'est le dernier point, on relie au premier
			         B = vaisseauEnnemie[0];

			     else           // sinon on relie au suivant.
			         B = vaisseauEnnemie[i+1];			     

			     D.x = B.x - A.x;
			     D.y = B.y - A.y;
			     T.x = P.x - A.x;
			     T.y = P.y - A.y;

			     float d = D.x*T.y - D.y*T.x;
			     	//std::cout<<d<<std::endl ;
			     if (d < 0)
			     	toucher = false ;

		  	}
		 }

		 if (toucher)
		 	tirDetruits.push_back(indTir) ;	 

		 indTir++ ; 
	}

	for (auto td : tirDetruits)
	{
		collisions++ ;std::cout<<"shoot"<<std::endl ;
		tirs.erase(tirs.begin()+td);
	}
}

std::vector<int> Vaisseau::CollisionTirBoule(std::vector<Boule> boules)
{
	int it = 0 , ib ;
	std::vector<int> tirDetruits ;
	std::vector<int> bouleTouches;
	for(auto t : tirs)
	{
		ib = 0 ;
		for(auto b : boules)
		{
			if (distanceEuclidienne(t.getPosition(),b.getPosition()) < b.getHauteur()/2)
			{
				tirDetruits.push_back(it);
				bouleTouches.push_back(ib);
			}
			ib++ ;	
		}
		it++ ;
	}

	for(auto td : tirDetruits)
		tirs.erase(tirs.begin()+td) ;

	return bouleTouches ;
}



void Vaisseau::CollisionBouleVaisseau(std::vector<Boule> boules)
{
	int i ;
	Point p ;
	for (auto b : boules)
	{
		p = b.getPosition();

		if (CollisionSegment(tabPoints[0],tabPoints[1],p, b.getHauteur()/2) ||
			CollisionSegment(tabPoints[1],tabPoints[2],p, b.getHauteur()/2) ||
			CollisionSegment(tabPoints[2],tabPoints[0],p, b.getHauteur()/2) )
		{
			//std::cout<<"toucher "<<p.x<<std::endl ;
		}
			

	}
}

bool Vaisseau::CollisionVaisseauVaisseau(std::vector<Point> autreV){


	int indi, indj ;
	for (int i = 0; i < tabPoints.size(); ++i)
	{
		for (int j = 0; j < autreV.size(); ++j)
		{
			if (i+1== tabPoints.size()) indi = 0 ; else indi = i+1 ;
			if (j+1== autreV.size()) indj = 0 ; else indj = j+1 ;
			if (CollisionSegSeg(tabPoints[i],tabPoints[indi],autreV[j],autreV[indj])) 
				return true ;
		}
	}

	return false ;
}
void Vaisseau::RenderAll(SDL_Surface** images,SDL_Surface* screen){
	/* for (std::list<Sprite>::iterator it = tirs.begin();
	        it!= tirs.end(); it++)
	 {
	 	std::cout<< *it->GetX()<<std::endl;

	 	//(*it)->Render(images,screen);
	 }*/ 

	 for(auto t : tirs)
	 	t.Render(images,screen) ;

   	Render(images,screen) ;
}


void Vaisseau::EvolueTir(){

	for (std::vector<Tir>::iterator t = tirs.begin();
	        t!= tirs.end(); t++)
	{
		/*if (t->getCalculTraj())
		{
			t->CalculAngle(t->getPointObj()) ;//std::cout<<distanceEuclidienne(t->getPosition(),t->getPointObj()) <<std::endl ;
			if (distanceEuclidienne(t->getPosition(),t->getPointObj()) < 100)
			{
				t->setCalculTraj(false) ;//std::cout<<"stop guidage" <<std::endl ;
			}
				
		}*/
		
		t->EvolueFromVector();
	}
		
	
}